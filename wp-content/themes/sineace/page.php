<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
	<section class="noticias">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-green">ÚLTIMAS NOTICIASsss</h2>
					<section class="latest-news">
						<div class="row">
							<div class="col-md-6 col-sm-6 box">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<a href="#" class="img-eventos" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/foto-evento.png');"></a>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h2>
											<a href="#">Institutos con carreras en atuoevaluación tienen hasta Setiembre</a>
										</h2>
										<div class="date"><small>19 de Setiembre de 2016</small></div>
										<article>
											<a href="#">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												</p>
											</a>
										</article>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 box">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<a href="#" class="img-eventos" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/foto-evento.png');"></a>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h2>
											<a href="#">Institutos con carreras en atuoevaluación tienen hasta Setiembre</a>
										</h2>
										<div class="date"><small>19 de Setiembre de 2016</small></div>
										<article>
											<a href="#">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												</p>
											</a>
										</article>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="col-md-6 col-sm-6 box">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<a href="#" class="img-eventos" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/foto-evento.png');"></a>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h2>
											<a href="#">Institutos con carreras en atuoevaluación tienen hasta Setiembre</a>
										</h2>
										<div class="date"><small>19 de Setiembre de 2016</small></div>
										<article>
											<a href="#">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												</p>
											</a>
										</article>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 box">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<a href="#" class="img-eventos" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/foto-evento.png');"></a>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h2>
											<a href="#">Institutos con carreras en atuoevaluación tienen hasta Setiembre</a>
										</h2>
										<div class="date"><small>19 de Setiembre de 2016</small></div>
										<article>
											<a href="#">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
													tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
													quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												</p>
											</a>
										</article>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-md-12">
					<h2 class="c-blue">GALERÍA DE IMAGENES FLIKCR</h2>
					<section class="gallery">
						<!-- gallery STRUCTURE -->
						<div class="niceGallery" id="gallery">
						<!-- HERE IS PLACE FOR PHOTOS -->
						</div>
						<!-- end gallery STRUCTURE --> 
					</section>
 
						
				</div>
			</div>
		</div>
	</section>
	<section class="youtube" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-red">ÚLTIMAS VIDEOS</h2>
					<div class="row">
						<div class="col-md-7">
							<section id="youtube-media-viewer">
								
							</section>
						</div>
						<div class="col-md-5">
							<div id="youtube">
						
							</div>	

							<div class="btns">
								<a href="javascript:;" id="youtube-prev-page" class="btn">
									<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
								</a>
								<a href="javascript:;" id="youtube-next-page" class="btn">
									<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>

	</section>
<?php endwhile; ?>
<?php get_footer(); ?>