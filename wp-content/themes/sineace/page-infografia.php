<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css?<?php echo rand(0, 100); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive-info.css?<?php echo rand(0, 100); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/estilos.css?v=<?php echo rand(0, 100); ?>">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery.min.js"></script>
    <link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon" >

	<meta property="og:locale" content="es_ES" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Sineace - Construyendo Calidad" />
	<meta property="og:description" content="Década de creación del Sineace, avances, logros y desafíos en el esfuerzo de construir una cultura de calidad en la educación peruana." />
	<meta property="og:url" content="https://www.sineace.gob.pe/construyendo-calidad/" />
	<meta property="og:image" content="https://www.sineace.gob.pe/construyendo-calidad/wp-content/uploads/2016/09/share-sineace.jpg" />
	<meta property="og:site_name" content="Sineace - Construyendo Calidad" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="Década de creación del Sineace, avances, logros y desafíos en el esfuerzo de construir una cultura de calidad en la educación peruana."/>
	<meta name="twitter:title" content="Sineace - Construyendo Calidad"/>


	<?php wp_head(); ?>
    <style type="text/css">
        html{
            margin-top: 0 !important
        }
    </style>
</head>
<body <?php body_class(); ?>>
		<section class="infografia">
			
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
						<div class="unslider">
							<ul>
								<li class="info1">
									<img src="<?php echo get_template_directory_uri() ?>/img/1.jpg" id="ele_1">

									<span class="abs animated zoomIn" id="book">
										<!-- <img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-libro.png" > -->
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/libro.gif" >
									</span>

									<span id="seis-point" class="animated zoomIn"></span>
									<span id="seis-point-red" class="animated zoomIn"></span>
									<span id="ocho-point-blue" class="animated zoomIn"></span>
									<span id="ocho-point-red" class="animated zoomIn"></span>

									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-one-gris.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-second-gris.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-one-blue.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span class="curve curve-blue-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-second-blue.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span class="abs animated zoomIn" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-2006.png" >
									</span>
									<span class="abs animated zoomIn" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-2008.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>
									</span>

									<span class="circle-red animated zoomIn" id="creacion">
										<a href="#creacion-del-sineace" class="various" data-color="red">
											<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-creacion-del-sineace.png">
										</a>
									</span>

									<span class="circle-red animated zoomIn" id="implementacion">
										<a href="#implementacion-sineace" class="various" data-color="blue">
											<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-implementacion.png">
										</a>
									</span>

									<!--PopUps-->
									<div class="hidden">

		                    <?php 

		                    query_posts(array( 
		                        'post_type' => 'infografias',
		                        'showposts' => 50,
		                        'order' => 'date',
		                        'orderby' => 'ASC'
		                    ) ); 
		                    $j=1;
		                    while (have_posts()) : the_post(); 

		                    ?>
		                    <?php if ($j%2!=0) { ?>								
										<div id="<?php echo( basename(get_permalink()) ); ?>" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')">
											<article class="hover-info red">
												<h2><?php the_title(); ?></h2>
												<?php the_content(); ?>
											</article>
										</div>

							<?php }else{ ?>

										<div id="<?php echo( basename(get_permalink()) ); ?>" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')">
											<article class="hover-info blue">
												<h2><?php the_title(); ?></h2>
												<?php the_content(); ?>
											</article>
										</div>								
							<?php } ?>

		                    <?php $j++; ?>
		                    <?php endwhile;?>
		                    <?php wp_reset_query(); ?>  



									</div>
									<span class="opa"></span>
								</li>

								<li class="info2">
									<img src="<?php echo get_template_directory_uri() ?>/img/2.png" id="ele_1">

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-certificación.gif" >
									</span>

									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curvas-roja.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-roja-2.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-roja-3.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-azul.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span id="seis-point" class="animated"></span>
									<span id="seis-point-red" class="animated"></span>
									<span id="ocho-point-blue" class="animated"></span>
									<span id="ocho-point-red" class="animated"></span>
									<span class="abs animated" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-2009.png" >
									</span>
									<span class="abs animated" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-2011.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
									</span>


									<span class="circle-red animated" id="creacion">
										<a href="#modelos-acreditacion" class="various" data-color="red">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-modelos.png">
										</a>
									</span>

									<span class="circle-red animated" id="implementacion">
										<a href="#avances-evidencias" class="various" data-color="blue">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-avances.png">
										</a>
									</span>
									<span class="opa"></span>
								</li>


								<li class="info3">
									<img src="<?php echo get_template_directory_uri() ?>/img/3.jpg" id="ele_1">

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-graduation.gif" >
									</span>
									<!--
									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-medalla.png" >
									</span> -->

									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-2.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-3.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-4.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-ploma-3.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span id="seis-point" class="animated"></span>
									<span id="seis-point-red" class="animated"></span>
									<span id="ocho-point-blue" class="animated"></span>
									<span id="ocho-point-red" class="animated"></span>
									<span class="abs animated" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-2012.png" >
									</span>
									<span class="abs animated" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-2013.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>

									</span>

									<span class="circle-red animated" id="creacion">
										<a href="#primeras-acreditaciones" class="various" data-color="red">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-acreditaciones.png">
										</a>
									</span>

									<span class="circle-red animated" id="implementacion">
										<a href="#pliego-presupuestal" class="various" data-color="blue">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-pliego.png">
										</a>
									</span>
									<span class="opa"></span>
								</li>

								<li class="info4">
									<img src="<?php echo get_template_directory_uri() ?>/img/4.jpg" id="ele_1">

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/edificio.gif" >
									</span>
								<!--
									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-birrete.png" >
									</span>

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-medalla.png" >
									</span>
								-->
									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-celeste-3.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-4.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-5.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-6.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span id="seis-point" class="animated"></span>
									<span id="seis-point-red" class="animated"></span>
									<span id="ocho-point-blue" class="animated"></span>
									<span id="ocho-point-red" class="animated"></span>
									<span class="abs animated" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-2014.png" >
									</span>
									<span class="abs animated" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-2015.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>

									</span>

									<span class="circle-red animated" id="creacion">
										<a href="#ley-universitaria" class="various" data-color="red">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-promulgacion.png">
									</a>
									</span>

									<span class="circle-red animated" id="implementacion">
										<a href="#politica-nacional" class="various" data-color="blue">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-politica.png">
									</a>
									</span>
									<span class="opa"></span>
								</li>
								<li class="info5">
									<img src="<?php echo get_template_directory_uri() ?>/img/5.jpg" id="ele_1">

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-leyes.gif" >
									</span>

									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-celeste-4.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-5.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-6.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-7.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span class="curve curve-blue-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-8.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span id="seis-point" class="animated"></span>
									<span id="seis-point-red" class="animated"></span>
									<span id="ocho-point-blue" class="animated"></span>
									<span id="ocho-point-red" class="animated"></span>
									<span class="abs animated" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-2016.png" >
									</span>
									<span class="abs animated" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-2016-2.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>
										<span class="six"></span>

									</span>

									<span class="circle-red animated" id="creacion">
										<a href="#renovacion-cambio" class="various" data-color="red">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-renovacion.png">
									</a>
									</span>

									<span class="circle-red animated" id="implementacion">
										<a href="#logros-y-resultados" class="various" data-color="blue">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-logros.png">
									</a>
									</span>
									<span class="opa"></span>
								</li>
								<li class="info6">
									<img src="<?php echo get_template_directory_uri() ?>/img/6.jpg" id="ele_1">

									<span class="abs animated" id="book">
										<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-compromiso.gif" >
									</span>

									<span class="curve curve-gris-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-azul-9.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-azul-10.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-gris-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-4.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-one">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-5.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span class="curve curve-blue-second">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-6.gif?v=<?php echo rand(0, 100); ?>" >
									</span>
									<span class="curve curve-blue-third">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-7.gif?v=<?php echo rand(0, 100); ?>" >
									</span>

									<span id="seis-point" class="animated"></span>
									<span id="seis-point-red" class="animated"></span>
									<span id="ocho-point-blue" class="animated"></span>
									<span class="abs animated" id="seis">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-2017.png" >
									</span>
									<span class="abs animated" id="ocho">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-2021.png" >
									</span>
									<span class="abs line-gray">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>
									</span>

									<span class="abs line-blue">
										<span class="one"></span>
										<span class="second"></span>
										<span class="third"></span>
										<span class="four"></span>
										<span class="five"></span>
										<span class="six"></span>

									</span>

									<span class="circle-red animated" id="creacion">
										<a href="#compromiso-futuro" class="various" data-color="red">
										<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-compromiso.png">
									</a>
									</span>

									<span class="opa"></span>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>

		</section>
		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/main.js?v=<?php echo rand(0, 100);?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/doT.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/codebird.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/animations.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/appear.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/hashtegny.js?v=2"></script>
        <script type="text/javascript">var site_url ="<?php echo site_url(); ?>";</script>
    </body>
</html>