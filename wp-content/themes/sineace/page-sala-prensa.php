<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
	<section class="noticias">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-green">Últimas noticias</h2>
					<section class="latest-news">
						<div class="row">

                    <?php 

                    query_posts(array( 
                        'post_type' => 'post',
                        'showposts' => 4,
                        'order' => 'date',
                        'orderby' => 'ASC'
                    ) ); 
                    $j=1;
                    while (have_posts()) : the_post(); 

                    ?>
							<div class="col-md-6 col-sm-6 box">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<a href="<?php echo get_field ("link"); ?>" target="_blank" class="img-eventos" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></a>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<h2>
											<a href="<?php echo get_field ("link"); ?>" target="_blank"><?php the_title(); ?></a>
										</h2>
										<div class="date"><small><?php echo get_the_date(); ?></small></div>
										<article>
											<a href="<?php echo get_field ("link"); ?>" target="_blank">
												<?php the_content(); ?>
											</a>
										</article>
									</div>
								</div>
							</div>
							<?php if ($j%2==0) { ?>
							<div class="clear"></div>
							<?php } ?>
                    <?php $j++; ?>
                    <?php endwhile;?>
                    <?php wp_reset_query(); ?>  

						</div>
					</section>
				</div>
				<div class="col-md-12">
					<h2 class="c-blue">Galería de imagenes</h2>
					<section class="gallery">
						<!-- gallery STRUCTURE -->
						<div class="niceGallery" id="gallery">
						<!-- HERE IS PLACE FOR PHOTOS -->
						</div>
						<!-- end gallery STRUCTURE --> 
					</section>
 
						
				</div>
			</div>
		</div>
	</section>
	<section class="youtube" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-red">Últimos vídeos</h2>
					<div class="row">
						<div class="col-md-7">
							<section id="youtube-media-viewer">
								
							</section>
						</div>
						<div class="col-md-5">
							<div id="youtube">
						
							</div>	

							<div class="btns">
								<a href="javascript:;" id="youtube-prev-page" class="btn">
									<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
								</a>
								<a href="javascript:;" id="youtube-next-page" class="btn">
									<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>

	</section>
<?php endwhile; ?>
<?php get_footer(); ?>