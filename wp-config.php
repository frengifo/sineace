<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'db_juego');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'J`,)uthar5vY.,g#%8Tx^D1h=!^Q9HqO4$ 5|1.NYFl1,&Dse:zf20s%|`Gj7Qw8');
define('SECURE_AUTH_KEY', '[{pW!e>*!C,CWxLb*wA1C>j[](Vh3%`IJ>H9Hyt:4KJ,TKgkD5*T^@#ib5 55pn6');
define('LOGGED_IN_KEY', 'e(D1#158_y~<K iZ JW2uQ_8`Y3wl#t`3-OPHh/Cy^H369X)DLHLe!KgS+q^uM#~');
define('NONCE_KEY', '-~t]IoSqf6g#TTbm{qbpBM[H97:]$I/qX.nEz:kC4Jeez;294M,uh^f(<lLnyQ~7');
define('AUTH_SALT', 'mz3@<?yqU;~:QlX<;/9O~5x+(x%AAkF~t)NXEv/RW]Y81R6`u >8vl^ZwRHLyM;i');
define('SECURE_AUTH_SALT', '_7++iq7g{|[VMX[O8^U:1f{& YY>AYx~Fs]v|,k@X^=b50*!7j_f-i>}3Mh)dH|u');
define('LOGGED_IN_SALT', 'U60nU2=;<A*2sgC]|Lmn{.(vA%ZbP^e{()T~[YZhq0%qL@Y;*TKj>}%lyXrDY/n,');
define('NONCE_SALT', 'aMH}{goOeCWn?,+:YK/5)oq%<m@.rRF!x9wUiJy!Y,`sS3Ujq%+Ev}Ebca+)^m:e');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wpsin_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

