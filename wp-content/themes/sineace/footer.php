		<footer>
            <section class="bg-white">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/logo-10anos.png">
                </a>
            </section>
            <section class="bg-blue">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="social pull-right">
                                <a href="https://www.facebook.com/SINEACEOFICIAL" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/SINEACEPERU" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="https://www.linkedin.com/company/sistema-nacional-de-evaluaci%C3%B3n-acreditaci%C3%B3n-y-certificaci%C3%B3n-de-la-calidad-educativa---sineace" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                <a href="https://www.youtube.com/user/SINEACE1" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                            </div>
                            <p>Sineace 2016 - Todos los derechos reservados</p>

                        </div>

                    </div>
                </div>
            </section>
        </footer>

        <?php wp_footer(); ?>
        
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/main.js?v=<?php echo rand(0, 100);?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/doT.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/codebird.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/animations.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/appear.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/hashtegny.js?v=2"></script>
        <script type="text/javascript">var site_url ="<?php echo site_url(); ?>";</script>
    </body>
</html>