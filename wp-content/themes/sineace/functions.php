<?php 
	//ACTIVA IMAGNE DESTACADA EN POST
	//add_theme_support( 'post-thumbnails', array( 'product' ) );
	//ACTIVA EL MENU
	function register_my_menu() {
	  register_nav_menu('header-menu',__( 'Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );
	add_theme_support( 'post-thumbnails' );

	//LIMITAR EXCERPT
	function get_excerpt($count){
	  $excerpt = get_the_excerpt();
	  $excerpt = strip_tags($excerpt);
	  $excerpt = substr($excerpt, 0, $count);
	  $excerpt = $excerpt.'...';
	  return $excerpt;
	}


	function pertel_scripts() {


	    if ( is_singular('page') ) {

			// Add Genericons, used in the main stylesheet.

			wp_enqueue_script( 'jquery-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array(), '2.1.5' );
			wp_enqueue_script( 'gallery-flickr', get_template_directory_uri().'/js/vendor/flickr/FlickrAPI.js', array(), '2.1.5');
			wp_enqueue_script( 'gallery-flickr-plugin', get_template_directory_uri().'/js/vendor/flickr/Flickr.Gallery.min.js', array(), '2.1.5');
			wp_enqueue_script( 'youtube', get_template_directory_uri().'/js/vendor/youtube-gallery/jquery.tubber.min.js', array(), '2.1.5');
			
			wp_enqueue_style( 'gallery-flickr', get_template_directory_uri().'/js/vendor/flickr/gallery.css', array(), '2.1.5');
			


			// Get the post type from the query



		}

		//if ( is_home()) {
			// Add Genericons, used in the main stylesheet.

			wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );

			wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');

			// Get the post type from the query
		//}

	}

	function twentyfourteen_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name', 'display' );

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );
	
	add_action( 'wp_enqueue_scripts', 'pertel_scripts' );