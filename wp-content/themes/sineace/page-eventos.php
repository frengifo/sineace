<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<section class="eventos" id="eventos">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-darkblue heading">Nuestros eventos</h2>
				</div>

                    <?php 

                    query_posts(array( 
                        'post_type' => 'eventos',
                        'showposts' => 20,
                        'order' => 'ASC'
                    ) ); 
                    $j=1;
					$c=0;
					$array = array("c-darkblue", "c-green", "c-blue", "c-red");

                    while (have_posts()) : the_post(); 

                    ?>

					<div class="col-md-4">
						<a href="<?php the_permalink(); ?>" class="img-eventos" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>">
						</a>
						<div class="row">							
							<div class="col-md-12">
								<span class="indice"><?php echo $j; ?></span>
								<article class="<?php echo $array[$c];  ?>">
									<strong class="fec ">
										<?php echo get_field("fecha_mostrada"); ?>  <br>
										<small><?php echo get_field("horario")?></small>
									</strong>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p class="excerpt">
										<?php echo get_field("descripcion_corta"); ?>
									</p>
									<h4 class="address">
										<?php echo get_field("direccion")?>
									</h4>
								</article>
							</div>
						</div>
					</div>

				<?php if ($j%3==0) { ?>
							<div class="clear"></div>
							<?php } ?>

                    <?php $j++; 
                    $c++;
                    if ($c==4){$c=0;} 
                    ?>
                    <?php endwhile;?>
                    <?php wp_reset_query(); ?>   

			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php get_footer(); ?>