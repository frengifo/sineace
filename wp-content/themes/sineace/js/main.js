jQuery(document).ready(function( $ ){ 

  if ( $(".various").length > 0 ) {
    jQuery(".various").fancybox({
      maxWidth  : 960,
      maxHeight : 540,
      fitToView : false,
      width   : '100%',
      height    : '90%',
      autoSize  : false,
      closeClick  : false,
      helpers     : { 
          overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
      },
      openEffect  : 'none',
      closeEffect : 'none',
      padding: 0,
      tpl:{
        closeBtn : '<a title="Close" class="hidden fancybox-item fancybox-close" href="javascript:;"></a>',
      },
      afterClose : function(){
        $("header").css({"z-index":"9999"});
        $(".fancybox-close").addClass('hidden');
      }
    });

  }

  if ( $(".form-contact").length > 0) {
    jQuery(".form-contact").fancybox({
      maxWidth  : 450,
      maxHeight : 540,
      fitToView : false,
      width   : '80%',
      height    : '90%',
      autoSize  : false,
      closeClick  : false,
      helpers     : { 
          overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
      },
      openEffect  : 'none',
      closeEffect : 'none',
      padding: 0,
      tpl:{
        closeBtn : '<a title="Close" class="hidden fancybox-item fancybox-close close-contact" href="javascript:;"></a>',
      },
      afterClose : function(){
        $("header").css({"z-index":"9999"});
        $(".fancybox-close").addClass('hidden');
      }
      
    });
  }


  $(".various, .form-contact").on("click", function(){
    $("header").css({"z-index":"999"});
    var color = $(this).data("color");
    setTimeout( function(){
      $(".fancybox-close").addClass(color);
      $(".fancybox-close").removeClass('hidden');
    }, 500);
  })

  $(".form-contact").on("click", function(){
    $("header").css({"z-index":"999"});
  })
  var slider = $(".unslider").unslider({

    nav : true,
    dots:false,
    arrows: {
      prev: '<a class="unslider-arrow prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
      next: '<a class="unslider-arrow next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',  
    },
    autoplay:false,
    delay:6000,
    });

  slider.on('unslider.change', function(event, index, slide) {
    /*Cambiamos*/  
    /*$(".unslider-active").find(".start").removeClass("start");
    $(".unslider-active").find(".animated").addClass("zoomIn");*/
    if (index == 1) {
      var clasIndex = "info2";
      $(".unslider-active").addClass( clasIndex );
      
      

      if ( !$(".unslider-active."+clasIndex).hasClass("loaded") )  {

        $(".unslider-active."+clasIndex).find(".animated").addClass("zoomIn");
        $(".info2 .one").addClass("start");
        $(".info2 .second").addClass("start");

        $('.info2 #seis, .info2 #book, .info2 #creacion, .info2 #ocho, .info2 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
            $(this).removeClass("zoomIn") 
            $(".info2 .one, .info2 .second,.info2 .third,.info2 .four, .info2 .five").addClass("start") ;
            setTimeout( function(){ $(".info2 .curve-gris-one").addClass("start") ;}, 0)
            setTimeout( function(){ $(".info2 .curve-gris-second").addClass("start") ;}, 4000)
            setTimeout( function(){ $(".info2 .curve-gris-third").addClass("start") ;}, 6000)
            setTimeout( function(){ $(".info2 .curve-blue-one").addClass("start") ;}, 4000)

        });
      }
      $(".unslider-active."+clasIndex).addClass("loaded");

    }

    if (index == 2) {
      var clasIndex = "info3";
      $(".unslider-active").addClass( clasIndex );
      
      

      if ( !$(".unslider-active."+clasIndex).hasClass("loaded") )  {

        $(".unslider-active."+clasIndex).find(".animated").addClass("zoomIn");
        $(".info3 .one").addClass("start");
        $(".info3 .second").addClass("start");

        $('.info3 #seis, .info3 #book, .info3 #creacion, .info3 #ocho, .info3 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
            $(this).removeClass("zoomIn") 
            $(".info3 .one, .info3 .second,.info3 .third,.info3 .four, .info3 .five").addClass("start") ;
            setTimeout( function(){ $(".info3 .curve-gris-one").addClass("start") ;}, 1500)
            setTimeout( function(){ $(".info3 .curve-gris-second").addClass("start") ;}, 4000)
            setTimeout( function(){ $(".info3 .curve-gris-third").addClass("start") ;}, 6000)
            setTimeout( function(){ $(".info3 .curve-blue-one").addClass("start") ;}, 1000)
        });
      }
      $(".unslider-active."+clasIndex).addClass("loaded");

    }

    if (index == 3) {
      var clasIndex = "info4";
      $(".unslider-active").addClass( clasIndex );
      
      

      if ( !$(".unslider-active."+clasIndex).hasClass("loaded") )  {

        $(".unslider-active."+clasIndex).find(".animated").addClass("zoomIn");
        $(".info4 .one").addClass("start");
        $(".info4 .second").addClass("start");

        $('.info4 #seis, .info4 #book, .info4 #creacion, .info4 #ocho, .info4 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
            $(this).removeClass("zoomIn") 
            $(".info4 .one, .info4 .second,.info4 .third,.info4 .four,.info4 .five").addClass("start") ;
            setTimeout( function(){ $(".info4 .curve-gris-one").addClass("start") ;}, 1500)
            setTimeout( function(){ $(".info4 .curve-gris-second").addClass("start") ;}, 1000)
            setTimeout( function(){ $(".info4 .curve-gris-third").addClass("start") ;}, 4000)
            setTimeout( function(){ $(".info4 .curve-blue-one").addClass("start") ;}, 7000)
        });
      }
      $(".unslider-active."+clasIndex).addClass("loaded");

    }

    if (index == 4) {
      var clasIndex = "info5";
      $(".unslider-active").addClass( clasIndex );
      
      

      if ( !$(".unslider-active."+clasIndex).hasClass("loaded") )  {

        $(".unslider-active."+clasIndex).find(".animated").addClass("zoomIn");
        $(".info5 .one").addClass("start");
        $(".info5 .second").addClass("start");

        $('.info5 #seis, .info5 #book, .info5 #creacion, .info5 #ocho, .info5 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
            $(this).removeClass("zoomIn") 
            $(".info5 .one, .info5 .second,.info5 .third,.info5 .four,.info5 .five, .info5 .six").addClass("start") ;
            setTimeout( function(){ $(".info5 .curve-gris-one").addClass("start") ;}, 1500)
            setTimeout( function(){ $(".info5 .curve-gris-second").addClass("start") ;}, 0)
            setTimeout( function(){ $(".info5 .curve-gris-third").addClass("start") ;}, 3000)
            setTimeout( function(){ $(".info5 .curve-blue-one").addClass("start") ;}, 7000)
            setTimeout( function(){ $(".info5 .curve-blue-second").addClass("start") ;}, 7000)
        });
      }
      $(".unslider-active."+clasIndex).addClass("loaded");

    }

    if (index == 5) {
      var clasIndex = "info6";
      $(".unslider-active").addClass( clasIndex );
      
      

      if ( !$(".unslider-active."+clasIndex).hasClass("loaded") )  {

        $(".unslider-active."+clasIndex).find(".animated").addClass("zoomIn");
        $(".info6 .one").addClass("start");
        $(".info6 .second").addClass("start");

        $('.info6 #seis, .info6 #book, .info6 #creacion, .info6 #ocho, .info6 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
            $(this).removeClass("zoomIn") 
            $(".info6 .one, .info6 .second,.info6 .third,.info6 .four,.info6 .five, .info6 .six").addClass("start") ;
            setTimeout( function(){ $(".info6 .curve-gris-one").addClass("start") ;}, 1500)
            setTimeout( function(){ $(".info6 .curve-gris-second").addClass("start") ;}, 3500)
            setTimeout( function(){ $(".info6 .curve-gris-third").addClass("start") ;}, 0)
            setTimeout( function(){ $(".info6 .curve-blue-one").addClass("start") ;}, 2800)
            setTimeout( function(){ $(".info6 .curve-blue-second").addClass("start") ;}, 7000)
            setTimeout( function(){ $(".info6 .curve-blue-third").addClass("start") ;}, 7000)
        });
      }
      $(".unslider-active."+clasIndex).addClass("loaded");

    }

  });

 

  $('.info1 #seis, .info1 #book, .info1 #creacion, .info1 #ocho, .info1 #implementacion').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ 
      $(this).removeClass("zoomIn") 
      $(".info1 .second,.info1 .third,.info1 .four,.info1 .five").addClass("start") ;
      setTimeout( function(){ $(".info1 .curve-gris-one").addClass("start") ;}, 2800)
      setTimeout( function(){ $(".info1 .curve-blue-one").addClass("start") ;}, 900)
      setTimeout( function(){ $(".info1 .curve-blue-second").addClass("start") ;}, 7700)

      setTimeout( function(){$(".info1 .curve-gris-second").addClass("start") ; }, 4800)
  });

  $(".info1 .one").addClass("start");


  $('#nav-icon3').click(function(){
    $(this).toggleClass('open');
    $(this).next().slideToggle();
  });
  
  $(window).click(function() {
      
    $("nav.list-menu").slideUp();
    $("#nav-icon3").removeClass('open');

  });
  $("nav.list-menu, #nav-icon3, header form > div input").click(function(event){
      event.stopPropagation();
  });
 
 /*$(".form-contact").click(function(){
    $(".various").fancybox({
      maxWidth  : 960,
      maxHeight : 540,
      fitToView : false,
      width   : '100%',
      height    : '90%',
      autoSize  : false,
      closeClick  : false,
      helpers     : { 
          overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
      },
      openEffect  : 'none',
      closeEffect : 'none',
      padding: 0,
      tpl:{
        closeBtn : '<a title="Close" class="hidden fancybox-item fancybox-close" href="javascript:;"></a>',
      },
      afterClose : function(){
        $("header").css({"z-index":"9999"});
        $(".fancybox-close").addClass('hidden');
      }
    });
 })*/

  //$("style#layout-style").after('<link id="layout-style" rel="stylesheet" href="css/main.grid.css" type="text/css" />').remove(); 

  // start the plugin & pass your options
  $('#hashtegny-container').hashtegny({
    twitter:{
      enable: true,
      hashtag: "ConstruyendoCalidad",
      count: 7        // number of posts to be displayed
    },
    google:{
            enable: false,
           
        },
        instagram:{
            enable: false,
          
        },
        vk:{
            enable: false,
          
        },
    textLength: 300,            // max length of post content
    animate: 8,                 // time of animation for each post in seconds
    refresh: 120,                // add recent posts if any evey 60 seconds
    updateTime: 120,             // update post time every 60 second(1 min)
    template: site_url+"/wp-content/themes/hanan_sineace/default.html",      
    template_html:null,
    callback: function() {          // A callback function which will fire when all the posts are fetched and displayed
      console.log("Posts have been fetched and displayed");
    }, 
    background: false,               // add background image(i.e 'background.jpg'). Images are inside img folder.
    showError: false                 // show error alert box if any error encountered
  });

  $('#eventos .slick-eventos').slick({
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: '<a href="javascript:;" class="slick-next"><i class="fa fa-chevron-right "></i></a>',
    prevArrow: '<a href="javascript:;" class="slick-prev"><i class="fa fa-chevron-left"></i></a>',
    responsive: [
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 375,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('#register .slick-eventos').slick({
    dots: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: '<a href="javascript:;" class="slick-next"><i class="fa fa-chevron-right "></i></a>',
    prevArrow: '<a href="javascript:;" class="slick-prev"><i class="fa fa-chevron-left"></i></a>',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]

  });
  
  



  if ( $(".two-col").length >  0 ) {
    var i=0, j = 0;
    var colores = ["#155091", "#c1d62e", "#199ad6", "#c52033"];
    var li_1 = "", li_2="";
    var per_col = Math.ceil( $(".two-col li").length / 2 );
    $(".two-col ul").hide();
    $(".two-col li").each( function(index){

      
      var span = "<span>"+ (index+1) +"</span>";
      $( span ).appendTo( ".two-col li:eq("+index+")" );
      //$(".two-col li:eq("+index+")").css({ "color": colores[i] })

      j++;
      $(this).find("h3").css({ "color":colores[i] })
      var li = "<li>"+$(this).html()+"</li>";
      if ( j <= per_col ) {
        li_1 += li;
      }else{
        li_2 += li;
      }

      

      i++;
      if ( i == 4 ) { i=0;}
    })

    $("<ul class='col-md-6 col-sm-6 col-xs-12'>"+li_1+"</ul>").appendTo(".two-col");
    $("<ul class='col-md-6 col-sm-6 col-xs-12'>"+li_2+"</ul>").appendTo(".two-col");
  }

  if ( $("#gallery").length >  0 ) {

    $("#gallery").flickrGallery({
      
      Key: 'dd7e89c7f0c07a951c30b34d7a013486',
      //Secret
      Secret: 'd792124bcd9f09bb',
      //FLICKR user ID
      User: '130110901@N08',
      //Flickr PhotoSet ID
      PhotoSet: '72157674136682806',
      /*-- VIEWBOX SETTINGS --*/ 
      Speed   : 400,    //Speed of animations
      navigation  : 1,    //(true) <a href="http://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a> (arrows)
      keyboard  : 1,    //(true) Keyboard navigation 
      numberEl  : 1     //(true) Number elements

    });


  }

  if ( $("#youtube").length >  0 ) {
    
    $('#youtube').tubber({
      username: 'SINEACE1',
      apiKey: 'AIzaSyAUpOFBtzh_BMjGQEKdxTYOmXsm_Xdtyeg',
      playlistId: 'PLJHd3APbUx62zBZHvcsNuBvjoqvQPyzWb',
      itemsPerPage: 4,
      embedVideo: false,
      loadingControl: $('#example5-loading'),
      prevPageControl: $('#youtube-prev-page'),
      nextPageControl: $('#youtube-next-page'),
      mediaViewer: $('#youtube-media-viewer'),
      showVideoTitle: true,
      templates: {
        thumbnail: '<img src="{{image_medium}}">'
      }
    });

  }

})


