<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<section class="box-heading">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="font-size: 18px;">
					<a href="<?php echo site_url(); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Volver</a>
					<h1>
						<?php the_title(); ?>
					</h1>
					<h3>
						<?php echo get_field("fecha_mostrada"); ?>
					</h3>
				</div>
			</div>
		</div>
	</section>
	<section class="detail">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="horario pull-left">
						<h4>HORARIO</h4>
						<small><?php echo get_field("horario"); ?></small>
					</div>
					<div class="lugar pull-left">
						<h4>LUGAR</h4>
						<small><?php echo get_field("direccion"); ?></small>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="info">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<?php the_content(); ?>
					</article>
				</div>

    <?php 

		  $libro = new WP_Query( array( 'post__in' => get_field("libro"), 'post_type' => 'libros', 'order'=> 'ASC', 'orderby'=> 'date', 'showposts' => 2 ) ); 
          $j=0; 
          while ($libro->have_posts()) : $libro->the_post();           
    ?>				
				<div class="col-md-4">
					<?php if (get_field("es_libro")==0){ ?>
					<div class="libro">
						<img src="<?php echo get_field("portada");  ?>">
						<h3>
							<?php the_title(); ?>
						</h3>
					</div>
					<?php }else{ ?>

				<br>
				<a href="<?php echo get_field ("link"); ?>" target="_blank" class="img-eventos" style="background-image: url('<?php echo get_field("portada");  ?>');"></a>
				<?php } ?>
				</div>
    <?php $j++ ?>
    <?php endwhile;?>
    <?php wp_reset_query(); ?>  

				<div class="clear"></div>
				<div class="sharer col-md-12">
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
						<img src="<?php echo get_template_directory_uri() ?>/img/share-fb.png">
					</a>
					<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
						<img src="<?php echo get_template_directory_uri() ?>/img/share-tw.png">
					</a>
					<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
						<img src="<?php echo get_template_directory_uri() ?>/img/share-g.png">
					</a>
					<!--
					<a href="#">
						<img src="<?php echo get_template_directory_uri() ?>/img/share-pin.png">
					</a>
					-->
				</div>
			</div>
		</div>
	</section>

	<section class="register" id="register">
		
		<div class="container">
			<div class="row">
			<!--
				<div class="col-md-6">
					<h2 class="c-red">Formulario de inscripción</h2>
					<div class="box-form row">
						<div class="col-xs-6">
							<input type="text" name="" placeholder="Nombres">
						</div>
						<div class="col-xs-6">
							<input type="text" name="" placeholder="Apellidos">
						</div>
						<div class="col-xs-6">
							<input type="text" name="" placeholder="Edad">
						</div>
						<div class="col-xs-6">
							<select>
								<option value="">Género</option>
								<option value="F">Femenino</option>
								<option value="M">Masculino</option>
							</select>
						</div>
						<div class="col-md-12">
							<input type="email" name="" placeholder="Email">
						</div>
						<div class="col-xs-12">
							<input type="submit" name="send" value="REGISTRATE" >
						</div>
					</div>
				</div>
			-->
			<?php echo do_shortcode( '[contact-form-7 id="34" title="Formulario de inscripción"]' ); ?>			
				<div class="col-md-6">
					<h2 class="c-blue">Nuestros Ponentes</h2>
					<section class="eventos">
						<div class="row">
							<div class="col-md-12">
								<div class="slick-eventos">
							
    <?php 

//var_dump(get_field("ponentes"));

          $ponentes = new WP_Query( array( 'post__in' => get_field("ponentes"), 'post_type' => 'ponentes', 'order'=> 'ASC', 'orderby'=> 'date' ) ); 
          $j=0; 
          while ($ponentes->have_posts()) : $ponentes->the_post();           
    ?>								
									<div class="box">
										<a href="#" class="img-eventos" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
										</a>
										<div class="row">
											<div class="col-md-12">
												<article class="c-blue">
													<h3><?php the_title(); ?></h3>
													<p class="c-blue"><?php echo get_field("cargo"); ?></p>
													<?php if (get_field("facebook")!=""){ ?><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a><?php } ?>
													<?php if (get_field("twitter")!="") { ?><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a><?php } ?>
													<?php if (get_field("linkedin")!=""){ ?><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a><?php } ?>
													<?php if (get_field("youtube")!="") { ?><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a><?php } ?>
												</article>
											</div>
										</div>
									</div>
    <?php $j++ ?>
    <?php endwhile;?>
    <?php wp_reset_query(); ?>  

								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>


	</section>

	<section class="cronograma">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<h2 class="heading">Cronograma de actividades</h2>
					<article class="two-col">
						<!--
						<ul >
							<li><h3>1Acreditación </h3> texto descripcion del cronograma<strong>17:00</strong></li>
							<li><h3>2Inicio del evento </h3>texto descripcion del cronograma<strong>18:00</strong></li>
							<li><h3>3Presentación del Libro "Huellas: un hito para" trasnformar la educación artística en el Perú </h3>texto descripcion del cronograma<strong>18:00</strong></li>
							<li><h3>4Lorem ipsum dolor sit amet conectertur adipscing </h3>texto descripcion del cronograma<strong>19:00</strong></li>
							<li><h3>5Acreditación </h3>texto descripcion del cronograma<strong>17:00</strong></li>
							<li><h3>6Inicio del evento </h3>texto descripcion del cronograma<strong>18:00</strong></li>
							<li><h3>7Presentación del Libro "Huellas: un hito para" trasnformar la educación artística en el Perú </h3>texto descripcion del cronograma<strong>18:00</strong></li>
							<li><h3>8Lorem ipsum dolor sit amet conectertur adipscing </h3>texto descripcion del cronograma<strong>19:00</strong></li>
							<li><h3>9Lorem ipsum dolor sit amet conectertur adipscing </h3>texto descripcion del cronograma<strong>19:00</strong></li>
							<li><h3>10Acreditación </h3>texto descripcion del cronograma<strong>17:00</strong></li>
						</ul>
					-->
					<?php echo get_field("actividades"); ?>					
					</article>
					<div class="address">
						<h2>Dirección del evento: <span style="font-family: 'Gotham-Book';"><?php echo get_field("direccion"); ?></span></h2>
					</div>
				</div>

			</div>

		</div>
	</section>

<style type="text/css">
	#map{height: 400px; width: 100%;}
</style>

    <section class="c-mapa">
      <div id="map"></div>
      <script>
        function initMap() {
          //var myLatLng = {lat: -12.1275186, lng: -76.9753165};
          var myLatLng = {lat: <?php echo get_field("latitud"); ?>, lng: <?php echo get_field("longitud"); ?>};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatLng
          });
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: "<?php echo get_template_directory_uri(); ?>/img/marker.png",
            title: 'Visitanos',
            animation: google.maps.Animation.DROP,
          });
        }
      </script>
      <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6w1tQpuDMZffD-cPP6y5cTXw42rHF_Ag&callback=initMap">
      </script>
    </section>

<?php endwhile; ?>
<?php get_footer(); ?>