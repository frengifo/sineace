<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<section class="libro">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-red heading">Descarga los libros</h2>
				</div>

                    <?php 

                    query_posts(array( 
                        'post_type' => 'libros',
                        'showposts' => 50,
                        'order' => 'date',
                        'orderby' => 'DESC'
                    ) ); 
                    $j=1;
                    while (have_posts()) : the_post(); 

                    ?>
                    <?php if (get_field("es_libro")==0){ ?>
				<div class="col-md-6">
					<article>
						<div class="row">
							<div class="col-md-5 col-sm-5 col-xs-12">
								<img src="<?php echo get_field("portada"); ?>" alt="Titulo">
							</div>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<h2><?php the_title(); ?></h2>
								<p><?php the_content(); ?></p>
								<div class="btns">
									<a href="<?php echo site_url(); ?>/pronto" class="btn-download c-blue">Descarga disponible muy pronto</a> 
									<!-- 
									<a href="<?php echo get_field("descarga"); ?>" class="btn-download c-blue">Descargar</a> 
									<a href="<?php echo get_field("link"); ?>" class="btn-line c-red">Ver en línea</a>
									-->
								</div>
							</div>
						</div>
					</article>
				</div>

				<?php if ($j%2==0) { ?>
							<div class="clear"></div>
							<?php } ?>

                    <?php $j++; ?>
					<?php } ?>
                    <?php endwhile;?>
                    <?php wp_reset_query(); ?>  

			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php get_footer(); ?>