<?php get_header(); ?>
<?php 
//if(get_field("streaming")=="si"){ 
if(1==0){
?>
<section class="infografia">
	<iframe width="100%" height="550" src="https://www.youtube.com/embed/_wqlSlMyNws?autoplay=1" frameborder="0" allowfullscreen></iframe>
</section>
<?php }else{ ?>
<section class="infografia">
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<script>

					jQuery(document).ready(function($) {
						$('.snd').snd('<?php echo get_template_directory_uri() ?>/Posme.mp3' );
						setTimeout( function(){
							jQuery(".snd").find(".toggle-play").trigger("click");
						}, 1555)
					});
					
				</script>
				<div class="snd">
					<div class="toggle">
						<button class="toggle-play"><i class="fa fa-play" aria-hidden="true"></i></button>
						<button class="toggle-pause"><i class="fa fa-pause" aria-hidden="true"></i></button>
					</div>
					
				</div>
				<div class="unslider">
					<ul>
						<li class="info1">
							<img src="<?php echo get_template_directory_uri() ?>/img/1.jpg" id="ele_1">

							<span class="abs animated zoomIn" id="book">
								<!-- <img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-libro.png" > -->
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/libro.gif" >
							</span>

							<span id="seis-point" class="animated zoomIn"></span>
							<span id="seis-point-red" class="animated zoomIn"></span>
							<span id="ocho-point-blue" class="animated zoomIn"></span>
							<span id="ocho-point-red" class="animated zoomIn"></span>

							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-one-gris.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-second-gris.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-one-blue.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span class="curve curve-blue-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-curve-second-blue.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span class="abs animated zoomIn" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-2006.png" >
							</span>
							<span class="abs animated zoomIn" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-2008.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>
							</span>

							<span class="circle-red animated zoomIn" id="creacion">
								<a href="#creacion-del-sineace" class="various" data-color="red">
									<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-creacion-del-sineace.png">
								</a>
							</span>

							<span class="circle-red animated zoomIn" id="implementacion">
								<a href="#implementacion-sineace" class="various" data-color="blue">
									<img src="<?php echo get_template_directory_uri() ?>/img/infografia/1-implementacion.png">
								</a>
							</span>

							<!--PopUps-->
							<div class="hidden">

                    <?php 

                    query_posts(array( 
                        'post_type' => 'infografias',
                        'showposts' => 50,
                        'order' => 'date',
                        'orderby' => 'ASC'
                    ) ); 
                    $j=1;
                    while (have_posts()) : the_post(); 

                    ?>
                    <?php if ($j%2!=0) { ?>								
								<div id="<?php echo( basename(get_permalink()) ); ?>" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')">
									<article class="hover-info red">
										<h2><?php the_title(); ?></h2>
										<?php the_content(); ?>
									</article>
								</div>

					<?php }else{ ?>

								<div id="<?php echo( basename(get_permalink()) ); ?>" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>')">
									<article class="hover-info blue">
										<h2><?php the_title(); ?></h2>
										<?php the_content(); ?>
									</article>
								</div>								
					<?php } ?>

                    <?php $j++; ?>
                    <?php endwhile;?>
                    <?php wp_reset_query(); ?>  



							</div>
							<span class="opa"></span>
						</li>

						<li class="info2">
							<img src="<?php echo get_template_directory_uri() ?>/img/2.png" id="ele_1">

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-certificación.gif" >
							</span>

							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curvas-roja.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-roja-2.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-roja-3.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info2/curva-azul.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span id="seis-point" class="animated"></span>
							<span id="seis-point-red" class="animated"></span>
							<span id="ocho-point-blue" class="animated"></span>
							<span id="ocho-point-red" class="animated"></span>
							<span class="abs animated" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-2009.png" >
							</span>
							<span class="abs animated" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-2011.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
							</span>


							<span class="circle-red animated" id="creacion">
								<a href="#modelos-acreditacion" class="various" data-color="red">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-modelos.png">
								</a>
							</span>

							<span class="circle-red animated" id="implementacion">
								<a href="#avances-evidencias" class="various" data-color="blue">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-avances.png">
								</a>
							</span>
							<span class="opa"></span>
						</li>


						<li class="info3">
							<img src="<?php echo get_template_directory_uri() ?>/img/3.jpg" id="ele_1">

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-graduation.gif" >
							</span>
							<!--
							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-medalla.png" >
							</span> -->

							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-2.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-3.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-azul-4.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info3/curva-ploma-3.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span id="seis-point" class="animated"></span>
							<span id="seis-point-red" class="animated"></span>
							<span id="ocho-point-blue" class="animated"></span>
							<span id="ocho-point-red" class="animated"></span>
							<span class="abs animated" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-2012.png" >
							</span>
							<span class="abs animated" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-2013.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>

							</span>

							<span class="circle-red animated" id="creacion">
								<a href="#primeras-acreditaciones" class="various" data-color="red">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-acreditaciones.png">
								</a>
							</span>

							<span class="circle-red animated" id="implementacion">
								<a href="#pliego-presupuestal" class="various" data-color="blue">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-pliego.png">
								</a>
							</span>
							<span class="opa"></span>
						</li>

						<li class="info4">
							<img src="<?php echo get_template_directory_uri() ?>/img/4.jpg" id="ele_1">

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/edificio.gif" >
							</span>
<!--
							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/3-birrete.png" >
							</span>

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/2-medalla.png" >
							</span>
-->
							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-celeste-3.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-4.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-5.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info4/curva-ploma-6.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span id="seis-point" class="animated"></span>
							<span id="seis-point-red" class="animated"></span>
							<span id="ocho-point-blue" class="animated"></span>
							<span id="ocho-point-red" class="animated"></span>
							<span class="abs animated" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-2014.png" >
							</span>
							<span class="abs animated" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-2015.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>

							</span>

							<span class="circle-red animated" id="creacion">
								<a href="#ley-universitaria" class="various" data-color="red">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-promulgacion.png">
							</a>
							</span>

							<span class="circle-red animated" id="implementacion">
								<a href="#politica-nacional" class="various" data-color="blue">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/4-politica.png">
							</a>
							</span>
							<span class="opa"></span>
						</li>
						<li class="info5">
							<img src="<?php echo get_template_directory_uri() ?>/img/5.jpg" id="ele_1">

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-leyes.gif" >
							</span>

							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-celeste-4.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-5.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-6.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-7.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span class="curve curve-blue-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info5/curva-azul-8.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span id="seis-point" class="animated"></span>
							<span id="seis-point-red" class="animated"></span>
							<span id="ocho-point-blue" class="animated"></span>
							<span id="ocho-point-red" class="animated"></span>
							<span class="abs animated" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-2016.png" >
							</span>
							<span class="abs animated" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-2016-2.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>
								<span class="six"></span>

							</span>

							<span class="circle-red animated" id="creacion">
								<a href="#renovacion-cambio" class="various" data-color="red">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-renovacion.png">
							</a>
							</span>

							<span class="circle-red animated" id="implementacion">
								<a href="#logros-y-resultados" class="various" data-color="blue">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/5-logros.png">
							</a>
							</span>
							<span class="opa"></span>
						</li>
						<li class="info6">
							<img src="<?php echo get_template_directory_uri() ?>/img/6.jpg" id="ele_1">

							<span class="abs animated" id="book">
								<img src="<?php echo get_template_directory_uri() ?>/img/gif/gif-compromiso.gif" >
							</span>

							<span class="curve curve-gris-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-azul-9.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-azul-10.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-gris-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-4.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-one">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-5.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span class="curve curve-blue-second">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-6.gif?v=<?php echo rand(0, 100); ?>" >
							</span>
							<span class="curve curve-blue-third">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/info6/curva-roja-7.gif?v=<?php echo rand(0, 100); ?>" >
							</span>

							<span id="seis-point" class="animated"></span>
							<span id="seis-point-red" class="animated"></span>
							<span id="ocho-point-blue" class="animated"></span>
							<span class="abs animated" id="seis">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-2017.png" >
							</span>
							<span class="abs animated" id="ocho">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-2021.png" >
							</span>
							<span class="abs line-gray">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>
							</span>

							<span class="abs line-blue">
								<span class="one"></span>
								<span class="second"></span>
								<span class="third"></span>
								<span class="four"></span>
								<span class="five"></span>
								<span class="six"></span>

							</span>

							<span class="circle-red animated" id="creacion">
								<a href="#compromiso-futuro" class="various" data-color="red">
								<img src="<?php echo get_template_directory_uri() ?>/img/infografia/6-compromiso.png">
							</a>
							</span>

							<span class="opa"></span>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>

</section>
<?php } ?>

<section class="eventos" id="eventos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="heading">Nuestros eventos</h2>
			</div>
			<div class="col-md-12">
				<div class="slick-eventos">
					
                    <?php 

                    query_posts(array( 
                        'post_type' => 'eventos',
                        'showposts' => 20,
                        'order' => 'ASC'
                    ) ); 
                    $j=1;
					$c=0;
					$array = array("c-darkblue", "c-green", "c-blue", "c-red");

                    while (have_posts()) : the_post(); 

                    ?>

					<div class="box">
						<a href="<?php the_permalink(); ?>" class="img-eventos" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>">
						</a>
						<div class="row">							
							<div class="col-md-12">
								<span class="indice"><?php echo $j; ?></span>
								<article class="<?php echo $array[$c];  ?>">
									<strong class="fec ">
										<?php echo get_field("fecha_mostrada"); ?>  <br>
										<small><?php echo get_field("horario")?></small>
									</strong>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p class="excerpt">
										<?php echo get_field("descripcion_corta"); ?>
									</p>
									<h4 class="address">
										<?php echo get_field("direccion")?>
									</h4>
								</article>
							</div>
						</div>
					</div>
                    <?php $j++; 
                    $c++;
                    if ($c==4){$c=0;} 
                    ?>
                    <?php endwhile;?>
                    <?php wp_reset_query(); ?>  

				</div>
			</div>
		</div>
	</div>
</section>

<section class="mix">
	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h2 class="heading">
					Tagboard #ConstruyendoCalidad
				</h2>
				<div class="row">
					<!--<div class="col-md-6 col-sm-6 col-xs-6">
						<article>
						    <img class="stick" src="<?php echo get_template_directory_uri() ?>/img/twitter-bad.png">
						    <img src="<?php echo get_template_directory_uri() ?>/img/icono-logo.png" class="pull-left">
							<h4>SINEACE <br> <small>@SINEACEPERU</small></h4>
							<div class="clear"></div>
							<p>
								Gracias al trabajo de @agrorural y #SINEACE, 350 agricultores han recibido la #certificación de sus competencias
							</p>
							<hr>
							<div class="date">15 Sep 5:59pm</div>
						</article> 
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<article>
						    <img class="stick" src="<?php echo get_template_directory_uri() ?>/img/twitter-bad.png">
						    <img src="<?php echo get_template_directory_uri() ?>/img/icono-logo.png" class="pull-left">
							<h4>SINEACE <br> <small>@SINEACEPERU</small></h4>
							<div class="clear"></div>
							<p>
								Gracias al trabajo de @agrorural y #SINEACE, 350 agricultores han recibido la #certificación de sus competencias
							</p>
							<hr>
							<div class="date">15 Sep 5:59pm</div>
						</article> 
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<article>
						    <img class="stick" src="<?php echo get_template_directory_uri() ?>/img/twitter-bad.png">
						    <img src="<?php echo get_template_directory_uri() ?>/img/icono-logo.png" class="pull-left">
							<h4>SINEACE <br> <small>@SINEACEPERU</small></h4>
							<div class="clear"></div>
							<p>
								Gracias al trabajo de @agrorural y #SINEACE, 350 agricultores han recibido la #certificación de sus competencias
							</p>
							<hr>
							<div class="date">15 Sep 5:59pm</div>
						</article> 
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<article>
						    <img class="stick" src="<?php echo get_template_directory_uri() ?>/img/twitter-bad.png">
						    <img src="<?php echo get_template_directory_uri() ?>/img/icono-logo.png" class="pull-left">
							<h4>SINEACE <br> <small>@SINEACEPERU</small></h4>
							<div class="clear"></div>
							<p>
								Gracias al trabajo de @agrorural y #SINEACE, 350 agricultores han recibido la #certificación de sus competencias
							</p>
							<hr>
							<div class="date">15 Sep 5:59pm</div>
						</article> 
					</div>-->
					<style type="text/css">
						.mix article{
							height: 216px;
						}
						.mix .box-feed{
							display:  none;
						}
						.mix .box-feed.SINEACE{
							display: block;
						}
					</style>
					<div id="hashtegny-container">
					
					</div>

				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<h2 class="heading">
					Zona de descargas
				</h2>
				<section class="circles">
					<a href="<?php echo site_url(); ?>/descarga-libros" class="circle yellow">
						<span>Libros<br>
						<img src="<?php echo get_template_directory_uri() ?>/img/icono-libro.png" alt="Libros"></span>
					</a>
					<a href="<?php echo site_url(); ?>/pronto" class="circle blue">
						<span>Diapositivas<br>
						<img src="<?php echo get_template_directory_uri() ?>/img/icono-diapositivas.png" alt="Diapositivas"></span>
					</a>
					<a href="<?php echo site_url(); ?>/pronto" class="circle red">
						<span>Infografía<br>
						<img src="<?php echo get_template_directory_uri() ?>/img/icono-infografia.png" alt="Infografía"></span>
					</a>
				</section>
			</div>
		</div>
	</div>

</section>

<?php get_footer(); ?>