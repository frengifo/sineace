<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css?<?php echo rand(0, 100); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive-info.css?<?php echo rand(0, 100); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/estilos.css?v=<?php echo rand(0, 100); ?>">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery.min.js"></script>
    <link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon" >

        <meta property="og:locale" content="es_ES" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Sineace - Construyendo Calidad" />
        <meta property="og:description" content="Década de creación del Sineace, avances, logros y desafíos en el esfuerzo de construir una cultura de calidad en la educación peruana." />
        <meta property="og:url" content="https://www.sineace.gob.pe/construyendo-calidad/" />
        <meta property="og:image" content="https://www.sineace.gob.pe/construyendo-calidad/wp-content/uploads/2016/09/share-sineace.jpg" />
        <meta property="og:site_name" content="Sineace - Construyendo Calidad" />
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:description" content="Década de creación del Sineace, avances, logros y desafíos en el esfuerzo de construir una cultura de calidad en la educación peruana."/>
        <meta name="twitter:title" content="Sineace - Construyendo Calidad"/>


	<?php wp_head(); ?>
    <style type="text/css">
        html{
            margin-top: 0 !important
        }
    </style>
</head>

<body <?php body_class(); ?>>
	
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-box pull-right">
                        <a href="<?php echo site_url(); ?>" class="pull-right logo-decada"><img src="<?php echo get_template_directory_uri() ?>/img/logo-de-la-decada-sineace1.png" alt="Logo de la Decada - Sineace"> </a>
                        <form action="#" method="post" class="pull-left">
                            <span>Búsqueda</span>
                            <div class="pull-right">
                                <input type="text" name="search"><button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </form>
                        
                    </div>

                    <a href="<?php echo site_url(); ?>" class="logo pull-left">
                        <img src="<?php echo get_template_directory_uri() ?>/img/logo-sineace.png" alt="Logo Sineace"> 
                    </a>
                    <div id="nav-icon3" class="pull-right">
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                    <nav class="list-menu">
                        <ul>
                            <li>
                                <a href="<?php echo site_url(); ?>">Inicio</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url(); ?>/eventos">Eventos</a>
                            </li>
                            <li>
                                <a hreF="<?php echo site_url(); ?>/sala-prensa">Sala de Prensa</a>
                            </li>
                            <li>
                                <a href="#contactenos" class="form-contact">Contáctenos</a>
                            </li>
                        </ul>
                    </nav>
                    <nav>
                        <ul>
                            <li>
                                <a href="<?php echo site_url(); ?>">Inicio</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url(); ?>/eventos">Eventos</a>
                            </li>
                            <li>
                                <a hreF="<?php echo site_url(); ?>/sala-prensa">Sala de Prensa</a>
                            </li>
                            <li >
                                <a href="#contactenos" class="form-contact">Contáctenos</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </header>
    <div class="hidden">
        
    
        <div id="contactenos" class="register">
            
            <div class="">
                <div class="col-md-12">
                    <article class="box-form-contact">
                        <!--
                        <div class="box-form row">
                            <div class="col-xs-6">
                                <input type="text" name="" placeholder="Nombres">
                            </div>
                            <div class="col-xs-6">
                                <input type="text" name="" placeholder="Apellidos">
                            </div>
                            
                            <div class="col-md-12">
                                <input type="email" name="" placeholder="Email">
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="" placeholder="Asunto">
                            </div>
                            <div class="col-md-12">
                                <textarea placeholder="MEnsaje" rows="10"></textarea>
                            </div>
                            <div class="col-xs-12">
                                <input type="submit" name="send" value="ENVIAR" >
                            </div>
                        </div>
                    -->
                    <?php echo do_shortcode( '[contact-form-7 id="231" title="Contactenos"]' ); ?>                    
                    </article>
                </div>
            </div>
        </div>
    </div>