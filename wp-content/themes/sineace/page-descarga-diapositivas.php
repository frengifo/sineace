<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<section class="diapo libro">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="c-blue heading">DESCARGA LAS DIAPOSITIVAS</h2>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article>
						<h3>Evento: Lanzamiento de la década del Sineace Arte en la educación</h3>
						<h4 class="c-green">27 de setiembre de 2016</h4>
						<div class="row links-downloads">
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
						</div>
						
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article>
						<h3>Evento: Lanzamiento de la década del Sineace Arte en la educación</h3>
						<h4 class="c-green">27 de setiembre de 2016</h4>
						<div class="row links-downloads">
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
						</div>
						
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article>
						<h3>Evento: Lanzamiento de la década del Sineace Arte en la educación</h3>
						<h4 class="c-green">27 de setiembre de 2016</h4>
						<div class="row links-downloads">
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
						</div>
						
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article>
						<h3>Evento: Lanzamiento de la década del Sineace Arte en la educación</h3>
						<h4 class="c-green">27 de setiembre de 2016</h4>
						<div class="row links-downloads">
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-6">
								<div class="row">
									<div class="col-xs-12 col-sm-2 col-xs-2">
										<img src="<?php echo get_template_directory_uri() ?>/img/icono-descarga-diapo.png" alt="Titulo">
									</div>
									<div class="col-xs-12 col-sm-10 col-xs-10">
										<p>Tema: En qué momento se perdió la libertad para crear <br>
										Autor: Liliana Galván Oré</p>
										
										<a href="#" class="btn-download c-blue">Descargar</a>
										
									</div>
								</div>
							</div>
						</div>
						
					</article>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php get_footer(); ?>